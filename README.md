#luo (WIP)

A simple and easy to use UI library tailored for mobile games using the LÖVE engine.

##Installation

Drop the luo folder into your existing LÖVE project and require it using:

##

```lua
    luo = require("luo/init")
```

##

##Documentation

luo features **9** different UI elements:

* ###luo.bar
* ###luo.book
* ###luo.button
* ###luo.input
* ###luo.label
* ###luo.list
* ###luo.slider
* ###luo.switch
* ###luo.view

All constructors for these modules follow this format:

##

```lua
    button = luo.button:new({
        x = 0,
        y = 0,
        width = 64,
        height = 32,
        etc ....
    })
```

##

The following documentation goes over the properties that can be specified within the constructor for each of these modules.

#luo.bar
###Description:
  The bar allows users to view the progress of a certain task or goal.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the bar
*y* | The y-coordinate of the bar
*width* | The width of the bar
*height* | The height of the bar

#luo.book
###Description:
  Similar to the list, the book allows the user to view content that exceeds the boundaries of the window.
  The book, however, paginates its content similar to a physical book.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the book
*y* | The y-coordinate of the book
*width* | The width of the book
*height* | The height of the book

#luo.button
###Description:
  The button is a ubiquitous UI element that allows a user to trigger an event through a tap.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the button
*y* | The y-coordinate of the button
*width* | The width of the button
*height* | The height of the button
*text* | The text displayed by the button
*font* | The font used for the button's label

#luo.input
###Description:
  The input allows users to input text into the program.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the input
*y* | The y-coordinate of the input
*width* | The width of the input
*height* | The height of the input

#luo.label
###Description:
  The label presents text to the user.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the label
*y* | The y-coordinate of the label
*text* | The text displayed by the label
*parent* | The parent of the label (**must be a luo module**)

#luo.list
###Description:
  The list allows users to scroll through content that exceeds through the boundaries of the window.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the list
*y* | The y-coordinate of the list
*width* | The width of the list
*height* | The height of the list

#luo.slider
###Description:
  The slider allows users to set a value within a range.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the slider
*y* | The y-coordinate of the slider
*width* | The width of the slider
*height* | The height of the slider

#luo.switch
###Description:
  The switch allows users to toggle a value between two states, on or off.
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the switch
*y* | The y-coordinate of the switch
*width* | The width of the switch
*height* | The height of the switch

#luo.view
###Description:
  The view acts as container for other UI elements (including other views).
###Properties:
Property | Description
------------ | -------------
*x* | The x-coordinate of the view
*y* | The y-coordinate of the view
*width* | The width of the view
*height* | The height of the view
*scaleX* | The scaling in the x direction of the view
*scaleY* | The scaling in the y direction of the view
*rotation* | The rotation in radians of the view

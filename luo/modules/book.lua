
--[[
  Similar to the list, the book allows the user to view content that exceeds the boundaries of the window.
  The book, however, paginates its content similar to a physical book.
]]--

local lg = love.graphics
local u = util
local t = touch

local book = {}

book.__parent = nil
book.__index = book

function book:new(properties)
  local self = {}

  return setmetatable(self, book)
end

return book

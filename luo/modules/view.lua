
--[[
  The view acts as container for other UI elements (including other views).
]]--

local lg = love.graphics
local u = util
local t = touch

local view = {}

view.__parent = nil
view.__index = view

function view:new(properties)
  local self = {}

  self.x = properties.x or 0
  self.y = properties.y or 0

  self.width = properties.width or 400
  self.height = properties.height or 300

  self.scaleX = properties.scaleX or 1
  self.scaleY = properties.scaleY or 1

  self.rotation = properties.rotation or 0

  self.objects = {}
  self.coords = {}
  self.coordTrans = {}

  self.numKeys = 0

  return setmetatable(self, view)
end

function view:update()
  for key, object in pairs(self.objects) do
    object.x = (self.x + self.coords[key].x) + self.coordTrans[key].x
    object.y = (self.y + self.coords[key].y) + self.coordTrans[key].y
    object:update()
  end
end

function view:draw()

  lg.push()

  lg.translate(self.x + self.width * 0.5, self.y + self.height * 0.5)

  lg.scale(self.scaleX, self.scaleY)
  lg.rotate(self.rotation)

  lg.translate(-(self.x + self.width * 0.5), -(self.y + self.height * 0.5))

  lg.setColor(1, 1, 1)
  lg.rectangle("line", self.x, self.y, self.width, self.height)

  for key, object in pairs(self.objects) do
    object:draw()
  end

  lg.pop()

end

function view:add(object, key, x, y)

  local k
  if(not key) then
    self.numKeys = self.numKeys + 1
    k = tostring(self.numKeys)
  else
    k = tostring(key)
  end

  local X = x or 0
  local Y = y or 0

  self.objects[k] = object
  self.objects[k].x = self.x + X
  self.objects[k].y = self.y + Y

  self.coords[k] = {x = X, y = Y}
  self.coordTrans[k] = {x = 0, y = 0}
end

--[[
  Apply a transformation to an object within the view
]]--

function view:applyTransformation(key, x, y)
  self.coordTrans[key].x = x or 0
  self.coordTrans[key].y = y or 0
end

--[[
  Get an object from a view using a key
]]--

function view:get(key)
  return self.objects[key] or error("Attempting to get an object from a view using an invalid key")
end

--[[
  Get an object from a view using the object
]]--

function view:find(obj)

  for key, object in pairs(self.objects) do
    if(object == obj) then
      return obj
    end
  end

  return false
end

--[[
  Remove an object from a view using a reference
]]--

function view:remove(obj)
  for key, object in pairs(self.objects) do
    if (object == obj) then
      self.objects[key] = nil
      self.coords[key] = nil
      self.coordTrans[key] = nil
      break
    end
  end
  return false
end

--[[
  Remove an object from a view using a key
]]--

function view:delete(key)
  for k in pairs(self.objects) do
    if (k == key) then
      self.objects[key] = nil
      self.coords[key] = nil
      self.coordTrans[key] = nil
      break
    end
  end
  return false
end

function view:setScale(scale)
  self:setScaleX(scale)
  self:setScaleY(scale)
end

function view:setScaleX(scale)
  self.scaleX = scale
end

function view:setScaleY(scale)
  self.scaleY = scale
end

return view


--[[
  Buttons are a ubiquitous UI element that allow users to trigger events through a tap.
]]--

local lg = love.graphics
local u = util
local t = touch

local button = {}

button.__parent = nil
button.__index = button

function button:new(properties)

  local self = {}

  self.x = properties.x or 0
  self.y = properties.y or 0

  self.width = properties.width or 200
  self.height = properties.height or 45

  self.label = button.__parent.label:new({
    text = properties.text or "",
    parent = self,
    x = self.width * 0.5,
    y = self.height * 0.5,
    font = properties.font or lg:getFont()
  })

  self.touch = nil

  return setmetatable(self, button)
end


function button:update(dt)
  self.label:update(dt)
end

function button:draw()
  lg.setColor(1, 1, 1, 1)
  lg.rectangle("fill", self.x, self.y, self.width, self.height, 25, 25)
  self.label:draw()
end


-------- Mutator Methods ----------

function button:setText(text)
  self.label:setText(text)
end

function button:setDimensions(width, height)
  self:setWidth(width)
  self:setHeight(height)
end

function button:setWidth(width)
  self.width = width or self.width
end

function button:setHeight(height)
  self.height = height or self.height
end

function button:setPosition(x, y)
  self:setX(x)
  self:setY(y)
end

function button:setX(x)
  self.x = x or self.x
end

function button:setY(y)
  self.y = y or self.y
end

return button

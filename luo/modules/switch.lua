
--[[
  The switch allows users to toggle a value between two states, on or off.
]]--

local lg = love.graphics
local u = util
local t = touch

local switch = {}

switch.__parent = nil
switch.__index = switch

function switch:new(properties)
  local self = {}

  return setmetatable(self, switch)
end

return switch


--[[
  The input allows users to input text into the program.
]]--

local lg = love.graphics

local input = {}

input.__parent = nil
input.__index = input

function input:new(properties)
  local self = {}

  return setmetatable(self, input)
end

return input

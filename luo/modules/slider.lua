
--[[
  The slider allows users to set a value within a range.
]]--

local lg = love.graphics
local u = util
local t = touch

local slider = {}

slider.__parent = nil
slider.__index = slider

function slider:new(properties)
  local self = {}

  return setmetatable(self, slider)
end

return slider

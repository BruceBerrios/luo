
local lg = love.graphics

local label = {}

label.__parent = nil
label.__index = label

function label:new(properties)
  local self = {}

  self.text = properties.text or "label"
  self.parent = properties.parent or false

  self.font = properties.font or lg:getFont()

  self.fontWidth = self.font:getWidth(self.text)
  self.fontHeight = self.font:getHeight(self.text)

  self.cx = (properties.x or 0)
  self.cy = (properties.y or 0)

  if(self.parent) then

    self.x = self.parent.x + self.cx
    self.y = self.parent.x + self.cy
  else
    self.x = self.cx
    self.y = self.cy
  end

  return setmetatable(self, label)
end

function label:update()
  if(self.parent) then
    self.x = (self.parent.x + self.cx) - self.fontWidth * 0.5
    self.y = (self.parent.y + self.cy) - self.fontHeight * 0.5
  else
    self.x = self.cx
    self.y = self.cy
  end
end

function label:draw()
  lg.setColor(1, 0, 0)
  if(not self.parent) then
    lg.print(self.text, self.x, self.y)
  else
    lg.print(self.text, self.x, self.y)
  end
end

function label:setText(text)
  self.text = text
  self.fontWidth = self.font:getWidth(self.text)
  self.fontHeight = self.font:getHeight(self.text)
end

return label


--[[
  The bar allows users to view the progress of a certain task or goal.
]]--

local lg = love.graphics
local u = util
local t = touch

local bar = {}

bar.__parent = nil
bar.__index = bar

function bar:new(properties)
  local self = {}

  return setmetatable(self, bar)
end

return bar

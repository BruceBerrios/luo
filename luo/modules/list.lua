
--[[
  The list allows users to scroll through content that exceeds through the boundaries of the window.
]]--

local lg = love.graphics
local u = util
local t = touch

local list = {}

list.__parent = nil
list.__index = list

function list:new(properties)
  local self = {}

  return setmetatable(self, list)
end

return list

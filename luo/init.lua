
local path = "luo/" -- path to luo folder

--[[
  Disable any unused modules
  Warning: Disabling a module and attempting to use it will result in an error
]]--

local modules = {
  ["bar"] = true,
  ["book"] = true,
  ["button"] = true,
  ["input"] = true,
  ["label"] = true,
  ["list"] = true,
  ["slider"] = true,
  ["switch"] = true,
  ["view"] = true
}

local function load()

  local self = {}

  require(path .. "util/util")
  require(path .. "util/touch")

  for module, value in pairs(modules) do
    self[module] = value and require(path .. "modules/" .. module) or nil

    if(value) then
      self[module].__parent = self
    end

  end

  return self
end

return load()

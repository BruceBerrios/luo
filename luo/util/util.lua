--[[
  This file contains helper functions tailored for the modules included with luo.
  Some of the functions here have been borrowed from lume.
  https://github.com/rxi/lume/blob/master/lume.lua
]]--

local sqrt = math.sqrt
local pairs, ipairs = pairs, ipairs
local type, error = type, error
local remove = table.remove

util = {}

local function isArray(table)
  return (type(table) == "table" and x[table] ~= nil) and true or false
end

local function getIter(table)
  if(isArray(table)) then
    return ipairs
  end

  if(type(table) == "table") then
    return pairs
  end

  error("Table expected at param 'table' in function getIter(table) at util.lua")
end

function util:lerp(a, b, dt)
  local t = self:clamp(dt, 0, 1)
  return a + (b - a) * dt
end

--[[
  @desc
    Clamps a number between a range

  @param
    num - the number to test
    min - the lower bound of the range
    max - the upper bound of the range

]]--

function util:clamp(num, min, max)
  return (num < min) and (min) or ((num > max) and (max) or (num))
end

--[[
  @desc
    Converts a number from one range to another range

  @param
    num  -
    min  -
    max  -
    min2 -
    max2 -

]]--

function util:map(num, min, max, min2, max2)
  return ( num - min ) * ( ( max2 - min2 ) / ( max - min ) ) + min2
end

--[[
  @desc


  @param
    x  -
    y  -
    x2 -
    y2 -

]]--

function util:distance(x, y, x2, y2)

  local dx = (x2 - x)
  local dy = (y2 - y)

  return sqrt( ( (dx * dx) + (dy * dy) ) )
end

--[[

]]--

function util:collides(x, y, w, h, x2, y2, w2, h2)

end

--[[

]]--

function util:collidesObj(object, object2)
end

--[[

]]--

function util:touches(x, y, object)

end

--[[

]]--

function util:centerTo(object2, object)
  local centerX = object.width * 0.5 - object2.width * 0.5
  local centerY = object.height * 0.5 - object2.height * 0.5
  return centerX, centerY
end

--[[

]]--

function util:clear(table)
  local iter = getIter(table)

  for key in iter(table) do
    table[key] = nil
  end
end

--[[

]]--

function util:remove(table, key)
  local iter = getIter(table)

  for index, value in iter(table) do
    if value == key then

      if isArray(table) then
        remove(table, index)
        break
      else
        table[index] = nil
        break
      end

    end
  end

end

--[[

]]--


--[[
  A wrapper class for love's touch module
]]--

local lt = love.touch

touch = {}

touch.__index = touch

function touch:new(id)
  local self = {}

  self.id = id
  self.isTap = true

  self.expired = false

  return setmetatable(self, touch)
end

function touch:getPosition()
  local t = pcall(lt.getPosition, self.id)

  if(t) then
    return lt.getPosition(self.id)
  end

  return -1
end

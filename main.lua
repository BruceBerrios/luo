
local lg = love.graphics
local lt = love.timer
local lw = love.window

function love.load()

  luo = require("luo/init")

  lw.setTitle("luo")

  root = luo.view:new({
    width = 800,
    height = 600
  })

  local play = luo.button:new({
    text = "play"
  })

  local centerX, centerY = util:centerTo(play, root)

  local fps = luo.label:new({})

  root:add(play, "play", centerX, centerY)
  root:add(fps, "fps")

end

local timer = 0

function love.update(dt)
  timer = util:lerp(timer, .85, dt * 4)

  root:setScale(timer)

  root:update()

  root:get("fps"):setText(lt:getFPS())

end

function love.draw()
  root:draw()
end
